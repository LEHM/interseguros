# Reto Interseguros

## Requerimientos
- docker 
- docker compose
- node  ^12.x.x
- npm ^6.x.x
- jest

## Instalación
El archivo docker-compose.yml está parametrizado para que pueda recibir valores acorde a variables de entorno que se pueda ejecutar mediante terminal (export NODE_ENV=development) o que sean definidos mediante un archivo .env en la raíz del proyecto.
```sh
$ git clone https://gitlab.com/LEHM/interseguros.git
$ cd interseguro-test/
$ touch .env
$ source .env  
$ docker-compose up --detach --build
```
Después de realizar todo el proceso puede visitar la dirección http://<host-ip>:${SERVER_PORT}/documentation en su navegador para observar la documentación de los endpoints del backend. 
Para visitar el demo puede visitar la dirección http://<host-ip>:${WEBAPP_PORT}/ en su navegador.

