import Vue from "vue";
import VueRouter from "vue-router";

import  AppIndexView from "./pages";


Vue.use(VueRouter);

const routes = [
    {
      path       : "/",
      component  : AppIndexView, 
  }
  ];
const router = new VueRouter({
    routes 
  })

export default router