import axios from 'axios';

export const httpPipeline = (...args) => {
    return args.reduce((prev,cur) => (prev + cur ))
}

export const performHttpGetRequest = (baseUrl,params) => {
    if(params){
        return axios.get(baseUrl,{
            params
        })
    }
    return axios.get(baseUrl)
}

export const parseResponse = (body) => {
    console.log(typeof body);
    return typeof body == 'object' ?  "Póliza encontrada" : "Póliza no encontrada"
}