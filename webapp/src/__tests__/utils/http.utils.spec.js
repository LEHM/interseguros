import { httpPipeline, performHttpGetRequest, parseResponse} from '../../utils/http.utils'

describe("Unit test for http utilites",()=>{
    it("should return a composed url",()=>{
        const mockResult = 'http://localhost:5000/api/insurance/find'

        const httpPipelineResult = httpPipeline("http://localhost:5000/","api/insurance/","find")

        expect(httpPipelineResult).toBe(mockResult)
    })
    it("should perform a http request", async ()=>{
        const mockResult = 'http://localhost:5000/api/insurance/find'
        const mockParams = {
            policyNumber : 15
        }

        const {data} = await performHttpGetRequest(mockResult, mockParams)
        expect(typeof data).toEqual('string' || 'object')



    })
    it("should return a string with value 'Póliza encontrada'",() => {
        const mockInput = {
            foo: 'bar'
        }

        const result = parseResponse(mockInput)

        expect(result).toEqual("Póliza encontrada")
    })

    it("should return a string with value 'Póliza no encontrada'",() => {
        const mockInput = "Insurance was not found"

        const result = parseResponse(mockInput)
        
        expect(result).toEqual("Póliza no encontrada")
    })
})

