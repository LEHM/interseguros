export const  config  = () =>({
    PG_HOST: process.env.PG_HOST,
    PG_USERNAME : process.env.PG_USERNAME,
    PG_PASSWORD : process.env.PG_PASSWORD,
    PG_PORT : process.env.PG_PORT,
    PG_DATABASE : process.env.PG_DATABASE,
    HTTP_PORT : process.env.HTTP_PORT,
    SWAGGER_PREFIX : process.env.SWAGGER_PREFIX,
    API_PREFIX : process.env.API_PREFIX
})
