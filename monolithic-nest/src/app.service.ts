import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from, Observable } from 'rxjs';
import { map } from "rxjs/operators"
import { Repository } from 'typeorm';
import { InsuranceEntity } from './models/app.entity';
import { InsuranceDTO } from "./models/app.dto"

@Injectable()
export class AppService {

  constructor(
    @InjectRepository(InsuranceEntity)
    private readonly insuranceRepository : Repository<InsuranceEntity> ){

  }
  
  findInsurance(policyNumber   : number): Observable<InsuranceDTO | string> {
    return  from(this.insuranceRepository.findOne({
      where : {
        insurance_number : policyNumber
      }
    }))
      .pipe(
        map((insurance : InsuranceEntity) => {
          
          if(!insurance)
            return "Insurance was not found"
          else
            return new InsuranceDTO({
              benefactor :insurance.benefactor,
              contractor : insurance.contractor,
              id : insurance.id,
              insuranceNumber  : insurance.insurance_number,
              insured : insurance.insured,
              startDate : String(insurance.start_date)
            })
          
        })
      ) 
  }

}
