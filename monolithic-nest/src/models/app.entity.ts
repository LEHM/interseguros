import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name : "insurance"
})
export class InsuranceEntity {


    @PrimaryGeneratedColumn()
    id : number

    @Column({
        width : 10,
        
    })
    insurance_number: number

    @Column({
        length : 100
    })
    contractor: string

    @Column({
        length : 100
    })
    insured: string

    @Column({
        length : 100
    })
    benefactor: string

    @Column()
    start_date: Date

}