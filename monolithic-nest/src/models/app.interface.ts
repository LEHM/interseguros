export interface InsuranceInterface{
    
    id : number;
    
    insuranceNumber : number;

    contractor : string;

    insured : string;

    benefactor : string;

    startDate : string;
}