export class InsuranceDTO{
    id : number
    insuranceNumber : number
    contractor : string
    insured : string
    benefactor : string
    startDate : string   

    constructor(   insuranceObject    : Partial<InsuranceDTO> ){
        Object.assign(this,insuranceObject);
    }
}
