import { Controller, Get, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { AppService } from './app.service';
import { InsuranceDTO } from './models/app.dto';


@ApiTags("General")
@Controller("insurance")
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get("find")
  findInsurance(@Query("policyNumber") policyNumber   : number ) : Observable<InsuranceDTO | string> {
    return this.appService.findInsurance(policyNumber)
  }
}
