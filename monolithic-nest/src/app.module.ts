import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CONFIGURATION, DEFAULT_ENV_FILE } from './app.constants';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { config } from './config/env.config';
import { InsuranceEntity } from './models/app.entity';

@Module({
  imports: [ConfigModule.forRoot(
    process.env.NODE_ENV ?  {
      isGlobal: true,
      envFilePath : DEFAULT_ENV_FILE,
      load : [config]
    } : {
      isGlobal : true,
      ignoreEnvFile : true
    }
  ),TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject : [ConfigService],
    useFactory : (configService: ConfigService) => {
          return       {
            type : "postgres",
            username : configService.get<string>(CONFIGURATION.PG_USERNAME),
            password : configService.get<string>(CONFIGURATION.PG_PASSWORD),
            host : configService.get<string>(CONFIGURATION.PG_HOST),
            port : configService.get<number>(CONFIGURATION.PG_PORT),
            database : configService.get<string>(CONFIGURATION.PG_DATABASE),
            synchronize : true,
            entities : [InsuranceEntity],
            autoLoadEntities: true,

         }
  }
  }),
  TypeOrmModule.forFeature([InsuranceEntity])
  ],
  controllers : [AppController],
  providers : [AppService]
})
export class AppModule {}
